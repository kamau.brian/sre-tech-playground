# shellcheck disable=SC1113
#/bin/bash

# Define the namespace
NAMESPACE="production"

# Check if the namespace exists
if kubectl get namespace "$NAMESPACE" > /dev/null 2>&1; then
  echo "Namespace '$NAMESPACE' already exists."
else
  echo "Creating namespace '$NAMESPACE'."
  kubectl create namespace "$NAMESPACE"
fi

## Then Let's Create the MYSQL Service.
## Create Secret to store mysql root password.
kubectl -n production apply -f deployments/mysql-secret.yaml

## Create Mysql Deployment.
kubectl -n  production apply -f deployments/mysql-deployment.yaml

## Create Mysql Service.
kubectl -n production apply -f deployments/mysql-service.yaml

## Create Redis Deployment.
kubectl -n production apply -f deployments/redis-deployment.yaml

## Create Redis Service.
kubectl -n production apply -f deployments/redis-service-svc.yaml

## Create RabbitMQ Deployment.
kubectl -n production apply -f deployments/rabbitmq-deployment.yaml

## Create RabbitMQ Service.
kubectl -n production apply -f deployments/rabbitmq-service-svc.yaml

echo "Setting up our cluster.... Please wait for a minute......."

## Sleep for a few to make sure the pod is deployed
sleep 1m

## Let's Deploy the profile services and transactions services.
## Create the profile deployment.
kubectl -n production apply -f deployments/profile-services-deployment.yaml

## Create the profile service.
kubectl -n production apply -f deployments/profile-services-svc.yaml

## Create the Transaction Deployment
kubectl -n production apply -f deployments/transaction-services-deployment.yaml

## Create the Transaction Service.
kubectl -n production apply -f deployments/transaction-services-svc.yaml

echo "Completed Successfully!!"

## Ends